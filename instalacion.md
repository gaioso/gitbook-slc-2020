# Instalación de GitBook

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Contidos

- [GNU/Linux](#gnulinux)
- [Primeiro proxecto](#primeiro-proxecto)
- [Sitio estático](#sitio-estático)
- [Servidor local](#servidor-local)
- [Vídeos](#vídeos)
    - [Instalación de nodejs y npm](#instalación-de-nodejs-y-npm)
    - [Instalación de Gitbook y DocToc](#instalación-de-gitbook-y-doctoc)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GNU/Linux

Para proceder coa instalación de GitBook debemos executar os seguintes comandos:

> Ubuntu 18.04 (bionic beaver)

	# apt install nodejs npm
	# npm install -g gitbook gitbook-cli doctoc

## Primeiro proxecto

Se queremos crear un primeiro proxecto baleiro no directorio actual:

	$ gitbook init

## Sitio estático

Para crear o contido estático en formato HTML nun subdirectorio `_book`:

	$ gitbook build

## Servidor local

Para poder visualizar o contido creado no paso anterior, levantamos un servidor local:

	$ gitbook serve
	...
	Starting server ...
	Serving book on http://localhost:4000

## Vídeos

### Instalación de nodejs y npm

<iframe src="https://archive.org/embed/nodejs_install" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

### Instalación de Gitbook y DocToc

<iframe src="https://archive.org/embed/gitbook_install" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

