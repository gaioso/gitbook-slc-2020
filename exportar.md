# Exportar a outros formatos

## Requisitos previos

Para poder executar os seguintes comandos temos que ter instalado o aplicativo *Calibre*, e así poder facer uso da utilidade `ebook-convert`.

## Conversión a ePub

	$ gitbook epub ./ ./mybook.epub

## Conversión a PDF

	$ gitbook pdf ./ ./mybook.pdf

## Conversión a Mobi

	$ gitbook mobi ./ ./mybook.mobi

