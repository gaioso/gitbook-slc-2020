# Índice de contidos con DocToc

Con esta utilidade seremos capaces de crear de xeito automático unha táboa de contidos nun documento con formato markdown. Só teremos que inserir as etiquetas correspondentes no punto onde desexamos que apareza dito índice.

## Etiquetas

Por defecto, a táboa de contidos sitúase ao comezo do documento, e recolle os nomes das cabeceiras que aparecen no resto do documento.

	<!-- START doctoc -->
	<!-- END doctoc -->

A continuación, executamos o seguinte comando para que se insira de xeito automático o índice do documento:

	$ doctoc <ficheiro> --gitlab --title '## Índice de contidos'

## Máis información

[Repositorio en Github](https://github.com/thlorenz/doctoc)
