# Presentación de GitBook

## Orixe

GitBook foi lanzado como proxecto no ano 2014 baixo unha licenza Apache 2.0, co obxectivo de conseguir unha solución simple e moderna para a xestión de documentación. Está desenvolvido con JavaScript como unha libraría de NodeJS, e pode ser usado dende a liña de comandos.

## Situación actual

Tal e como se indica no [proxecto en Github](https://github.com/GitbookIO/gitbook), a día de hoxe o equipo de desenvolvedores orixinal deste proxecto está a traballar en [Gitbook.com](https://www.gitbook.com/), polo que o proxecto orixinal xa non se está actualizando.

## Vantaxes

* Os contidos son gardados nun formato baseado en texto plano, como é *markdown* ou *asciidoc*, o que facilita o seu tratamento de xeito illado, ou cunha ferramenta externa diferente a GitBook.
* Exportación sinxela e rápida a outros formatos (HTML, PDF, ePUB...)
* Integración cun sistema de control de versións. Publicación automática na nube.
